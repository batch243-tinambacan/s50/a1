/*
	- Syntax: npx create-react-app "project-name"
	- cd "project name"
	- Execute "npm start" to run our project in localhost
	- Delete unneccessary files from the newly created project
		1. App.test.js
		2. index.css
		3. logo.svg
		4. reportWebVitals.js
	- Error will be encountered: importation of the deleted files
		a. to fix it, remove the codes of the imported files
	- We now have a blank slate. We can start building our own ReactJS app


	Important Note:

	- Similar to nodeJS and expressJS we can install packages in our react app to make the work easier for us.
		example: npm install react-bootstrap bootstrap

	- The "import" statement allows us to use the code/exported modules from files similar to how we use the require keyword in NodeJS
		example - import this to index.js: import 'bootstrap/dist/css/bootstrap.min.css';

	- ReactJS components are independent, reusable pieces of code which normally contains JS and JSX syntax which make up a part of our app.
	-JSX combination of JS and html in our codes

*/

// [Components]
/*Create seperate components and then just mount the components in App.js inside return() instead of putting the whole code for readability and reusability*/

// {Fragment} to allow multiple elements in return function

// Storing info in  a context object is done by providing info using the corresponding "provider" component and passing the info via the "value" prop

// All info in the "provider" can be accessed later on from the context objects as properties