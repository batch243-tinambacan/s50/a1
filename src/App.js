// require('dotenv').config()

// [pages] imported
import AppNavBar from './components/AppNavBar'; 
import Home from './pages/Home'; // contains banner & highlights
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageError from './pages/PageError';
import CourseView from './pages/CourseView';

import {UserContext} from './UserContext';
import {UserProvider} from './UserContext'

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {useState, useEffect} from 'react';

import './App.css';




function App() {

  const [user, setUser] = useState({id: null, isAdmin: false}) // State hook globally accessible using useContext; will be used to store the users info for validating if a user is logged in or not

  const unSetUser = ()=>{   // function for clearing local storage when logged out
    localStorage.clear()
  }

  useEffect(()=>{
    console.log(user)
  }, [user])

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_URI}/users/profile`,
      {headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }})
    .then(response=> response.json())
    .then(data =>{
      console.log(data);
      setUser({id: data._id, isAdmin: data.isAdmin});
    })
  }, [])

  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <Router>
        <AppNavBar/>
        <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/courses" element={<Courses/>}/>
            <Route path="/courses/:courseId" element={<CourseView/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/> {/*element={!user ? <Login /> : <Navigate to="/" />}*/}
            <Route path="/logout" element={<Logout/>}/>
            <Route path="*" element={<PageError/>}/>
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
