import React from 'react';

const UserContext = React.createContext(); //contain the createContext method inside UserContext variable

export const UserProvider = UserContext.Provider; //access the Provider using dot notation

export default UserContext;

/* [Context object]
	- is a data type that can be used to store info that can be shared to other components.
	- it is another approach to passing info between components and allows easier access by the use of prop-drilling

	[Provider] 
	- component allows other component to use the context object and supp[ly neccessary info needed to the context object]
*/ 