/*import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';*/


// importing modules from using deconstructuring
import {Container, Nav, Navbar} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import {Fragment, useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavBar(){ // pwede nang gamitin outside of this file

	//state to store the user info in local storage
	// const[user, setUser] = useState(localStorage.getItem('email'));

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg" className="vw-100">
		    <Container fluid>
		      <Navbar.Brand as={NavLink} to="/">Course Booking</Navbar.Brand>
		      <Navbar.Toggle aria-controls="basic-navbar-nav" />
		      <Navbar.Collapse id="basic-navbar-nav">
		        <Nav className="ms-auto">
		          <Nav.Link as={NavLink} to="/" >Home</Nav.Link>
		          <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
		          {
		          	(user.id !== null) ? 
		          	<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
		          	: 
		          	<Fragment>
		          	<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
					<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
					</Fragment>
		          }
		        </Nav>
		      </Navbar.Collapse>
		    </Container>
		  </Navbar>
		)
}