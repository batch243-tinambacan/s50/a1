import {useState, useEffect, useContext} from 'react'

import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import {Link} from 'react-router-dom'


export default function CourseCard(prop){ //props - to contain the prop that we will receive; can use any param name but best to use props; If no curly braces, then need to use dot notation
	/*console.log(props);*/

	//destructuring the courseProp
	const {_id, name, description, price, slots} = prop.prop;

	// [STATE] use the state hook for this component to be able to store its state specifically to monitor the number of enrollees
	//states are used to keep track of info related to individual components
	// syntax: 
		// const [getter, setter] = useState(initialGetterValue)
		// getter is variable; setter is function

	const [enrollees, setEnrollees] = useState(0);
	const [slotsAvailable, setSlotsAvailable] = useState(slots)
	const [isAvailable, setIsAvailable] = useState(true);

	const {user} = useContext(UserContext)

	//Add an "useEffect" hook to have "CourseCard" componet do perform a certain task after every DOM update
		//Syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])

	useEffect(()=>{
		if(slotsAvailable === 0){
			setIsAvailable(false);
			console.log(isAvailable);
		}
	}, [slotsAvailable]);


	function enroll(){
		if(slotsAvailable === 1){
			alert("Congratulations, you were able to enroll before the cut!");
		}
		setEnrollees(enrollees+1);
		setSlotsAvailable(slotsAvailable-1);
	}
	

	// since enrollees is declare as a constant variable, directly reassigning the value is not allowed or will cause an error. Thus, we use enrollees+1 and not enrollees++ (increment is not applicable to const variable)

	return(
		<Row className = "mt-3">
			<Col xs = {12} md = {4} className = "offset-md-4 offset-0">
				<Card>
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>
				         	{description}
				        </Card.Text>

				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>
				         	{price}
				        </Card.Text>

				        <Card.Subtitle>Enrollees</Card.Subtitle>
				        <Card.Text>
				         	{enrollees}
				        </Card.Text>

				        <Card.Subtitle>Slots Available:</Card.Subtitle>
				        <Card.Text>
				         	{slotsAvailable} slots
				        </Card.Text>
				        	{
				        	(user !== null) ?
				        	<Button variant="primary"
					        	 as={Link} to={`/courses/${_id}`}
					        	 variant="primary"
					        	 disabled = {!isAvailable} 
					        	 >Details</Button>
				        	 :
						    <Button as={Link} to='/login' variant="primary" 
						       disabled={!isAvailable}
						       >Enroll</Button>
				        	}
				      </Card.Body>
				    </Card>
			</Col>
		</Row>

		)
}
