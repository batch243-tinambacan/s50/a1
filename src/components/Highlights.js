// import of the classes needed for the crc rule
import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
		{/*First Card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>
				       		<h2>Learn From Home</h2>
				       	</Card.Title>
				       <Card.Text>
				         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vestibulum nunc eget magna sagittis, id venenatis metus rutrum. Donec vulputate semper orci, quis finibus ligula. Quisque libero eros, iaculis nec enim in, tempus consectetur mauris. Nullam in justo sagittis, suscipit ante at, tempor est. Vivamus dignissim mollis dolor, at aliquet urna molestie quis. Quisque tincidunt, leo sed viverra pretium, dolor est tempor dolor, at placerat nibh arcu vitae erat. Sed id nisl sapien. Fusce lacinia lacinia consectetur. Morbi tortor lacus, efficitur ac sodales eu, mattis quis libero. Praesent quis leo lacinia, imperdiet ex a, ultricies sem. Integer fermentum, lorem in malesuada elementum, augue ex eleifend diam, efficitur pulvinar nisl neque vitae est. Aliquam eleifend laoreet finibus. Curabitur id augue nec nisi pellentesque venenatis.
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>
			{/*Second Card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>
				       		<h2>Study Now, Pay Later</h2>
				       	</Card.Title>
				       <Card.Text>
				         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vestibulum nunc eget magna sagittis, id venenatis metus rutrum. Donec vulputate semper orci, quis finibus ligula. Quisque libero eros, iaculis nec enim in, tempus consectetur mauris. Nullam in justo sagittis, suscipit ante at, tempor est. Vivamus dignissim mollis dolor, at aliquet urna molestie quis. Quisque tincidunt, leo sed viverra pretium, dolor est tempor dolor, at placerat nibh arcu vitae erat. Sed id nisl sapien. Fusce lacinia lacinia consectetur. Morbi tortor lacus, efficitur ac sodales eu, mattis quis libero. Praesent quis leo lacinia, imperdiet ex a, ultricies sem. Integer fermentum, lorem in malesuada elementum, augue ex eleifend diam, efficitur pulvinar nisl neque vitae est. Aliquam eleifend laoreet finibus. Curabitur id augue nec nisi pellentesque venenatis.
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>
			{/*Third Card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>
				       		<h2>Be Part of Our Community!</h2>
				       	</Card.Title>
				       <Card.Text>
				         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vestibulum nunc eget magna sagittis, id venenatis metus rutrum. Donec vulputate semper orci, quis finibus ligula. Quisque libero eros, iaculis nec enim in, tempus consectetur mauris. Nullam in justo sagittis, suscipit ante at, tempor est. Vivamus dignissim mollis dolor, at aliquet urna molestie quis. Quisque tincidunt, leo sed viverra pretium, dolor est tempor dolor, at placerat nibh arcu vitae erat. Sed id nisl sapien. Fusce lacinia lacinia consectetur. Morbi tortor lacus, efficitur ac sodales eu, mattis quis libero. Praesent quis leo lacinia, imperdiet ex a, ultricies sem. Integer fermentum, lorem in malesuada elementum, augue ex eleifend diam, efficitur pulvinar nisl neque vitae est. Aliquam eleifend laoreet finibus. Curabitur id augue nec nisi pellentesque venenatis.
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>
		</Row>
		)
}