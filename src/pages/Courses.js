import {Fragment, useEffect, useState} from 'react';
import CourseCard from '../components/CourseCard';
/*import courseData from '../data/courses.js'*/; // mock data

export default function Courses(){
	const [courses, setCourses] = useState([]);
	// [Get local storage] 
	//Syntax: localStorage.getItem('property name');

	/*const local = localStorage.getItem("email");
	console.log(local);*/

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URI}/courses/allActiveCourses`)
			.then(response => response.json())
			.then(data =>{
				console.log(data);

				setCourses(data.map(course => {
		return (
			<CourseCard key={course._id} prop={course} />
			)
			}))
		})	
	}, [])


	return(
		<Fragment>
			{courses}
		</Fragment>
		)
}

/* [Passing Property/Mock data to our CourseCard Component]

	-Import the data and the component that we will pass the props to
	-We have to add parameter to CourseCard to contain the property/data that we will receive
*/