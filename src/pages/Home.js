import Container from 'react-bootstrap/Container';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


export default function Home() {
	return(
		<Container>
		  <Banner/>
		  <Highlights/>
		</Container>
		)
}