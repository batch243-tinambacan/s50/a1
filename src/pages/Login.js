import {Form, Button, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false)
	const {user, setUser} = useContext(UserContext);

	useEffect(()=>{
		if (email !== "" && password !==""){
				setIsActive(true);
		}
		else{
			setIsActive(false)
		}
	}, [email,password])

	function loginUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_URI}/users/login`, { // fetch a request to the corresponding api
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email, // :email is the state hooks
				password: password
			})
		}).then(response => response.json())
		.then(data =>{

			console.log(data)

			if(data.accessToken !== "empty"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to our website!"
				})
			}
			else{
				Swal.fire({
					title: "Authentication failed :(",
					icon: "error",
					text: "Check your login details and try again."
				})
				setPassword('');
			}
		})

		const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_URI}/users/profile`,
				{headers: {
					Authorization: `Bearer ${token}`
				}})
			.then(response=> response.json())
			.then(data =>{
				console.log(data);
				setUser({id: data._id, isAdmin: data.isAdmin});
			})
		}
	}

	return(
		(user.id !== null) ?
			<Navigate to="/" />
		:
		<Container>
			<Row>
				<Col className="col-md-4 col-8 offset-md-4 offset-2">
					<Form onSubmit={loginUser} className="bg-secondary p-3">
					  
					  <Form.Group className="mb-3" controlId="email">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control 
					    	type="email" 
					    	placeholder="Enter email" 
					    	value = {email}
					    	onChange  = {event => setEmail(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="password">
					    <Form.Label>Enter your desired Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	value = {password}
					    	onChange  = {event => setPassword(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Button 
						  variant="primary" 
						  type="submit"
						  id="submitBtn"
						  disabled={!isActive}
						  >Login
					  </Button>
					</Form>
				</Col>
			</Row>
		</Container>	
		)
}