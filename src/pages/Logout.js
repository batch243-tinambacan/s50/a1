import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import {useContext, useEffect} from 'react';
import Swal from 'sweetalert2';

export default function Logout(){

	const {setUser, unSetUser} = useContext(UserContext);

	unSetUser();

	useEffect(()=>{
		setUser({id: null, isAdmin:false});
	}, [])
	
	Swal.fire({
		title: "You returned to the outside world :(",
		icon: "info",
		text: "Babalik ka rin <3"
	})

	return(
		<Navigate to="/login" />
		)
}

