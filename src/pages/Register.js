import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Container, Row, Col} from 'react-bootstrap';

import {useNavigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){
	// [State hooks to store the value of input]
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [password2, setPassword2] = useState("");


	const [isActive, setIsActive] = useState(false)

	const {user, setUser} = useContext(UserContext);

	const navigateToLogin = useNavigate();

	useEffect(()=>{
		if (firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !=="" && password2 !=="" && password===password2 && mobileNo.length==11){
				setIsActive(true);
		}
		else{
			setIsActive(false)
		}

	}, [firstName, lastName, email, mobileNo, password, password2])


	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_URI}/users/register`, {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email, // :email is the state hooks
				mobileNo: mobileNo,
				password: password,
				password2: password2,
			})
		}).then(response => response.json())
		.then(data =>{
			console.log(data);

			if(data.emailExists) {
				Swal.fire({
					title: "Email already exist",
					icon: "error",
					text: "Please use a different email"
				})
			}
			else{

					Swal.fire({
						title: "Registered Successfully!",
						icon: "success",
						text: "Please login."
					})
					navigateToLogin("/login");
				}
			})
	}


	return(
		<Container>
			<Row>
				<Col className="col-md-4 col-8 offset-md-4 offset-2">
					<Form onSubmit={registerUser} className="bg-secondary p-3">

					  <Form.Group className="mb-3" controlId="firstName">
					    <Form.Label>First Name</Form.Label>
					    <Form.Control 
					    	type="name" 
					    	placeholder="Enter First Name" 
					    	value = {firstName}
					    	onChange  = {event => setFirstName(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="lname">
					    <Form.Label>Last Name</Form.Label>
					    <Form.Control 
					    	type="name" 
					    	placeholder="Enter Last Name" 
					    	value = {lastName}
					    	onChange  = {event => setLastName(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="email">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control 
					    	type="email" 
					    	placeholder="Enter email" 
					    	value = {email}
					    	onChange  = {event => setEmail(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="mobileNumber">
					    <Form.Label>Mobile Number</Form.Label>
					    <Form.Control 
					    	type="number" 
					    	placeholder="Enter mobile number" 
					    	value = {mobileNo}
					    	onChange  = {event => setMobileNo(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="password">
					    <Form.Label>Enter your desired Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	value = {password}
					    	onChange  = {event => setPassword(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="password2">
					    <Form.Label>Verify your Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password"
					    	value = {password2}
					    	onChange  = {event => setPassword2(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Button 
						  variant="primary" 
						  type="submit"
						  disabled={!isActive}
						  >Register
					  </Button>
					</Form>
				</Col>
			</Row>
		</Container>
	)

}